﻿using BankManagement_MockAPI.Model;
using BankManagement_MockAPI.Repository;
using Newtonsoft.Json;
using System.Net.Http.Json;

namespace BankManagement_MockAPI.Controller
{
    public class Login : UrlRepository
    {
        List<Customer> customers;
        public Login()
        {
            customers = new List<Customer>();
        }
        public async Task GetData(HttpClient client)
        {
            string urlPath = string.Concat(UrlRepository.url, UrlRepository.endPoint);
            HttpResponseMessage response = await client.GetAsync(urlPath);
            string data = await response.Content.ReadAsStringAsync();
            customers = JsonConvert.DeserializeObject<List<Customer>>(data);
        }
        public bool LogIn(string account, string pin)
        {
            foreach (var customer in customers)
            {
                if (!customer.Account.Equals(account) && !customer.Pin.Equals(pin))
                {
                    continue;
                }
                if (!customer.Account.Equals(account))
                {
                    throw new Exception("Account does not exist !");
                }
                if (!customer.Pin.Equals(pin))
                {
                    throw new Exception("Wrong password !");
                }
            }
            return true;
        }
        public Customer GetCustomer(string account, string pin)
        {
            foreach (var customer in customers)
            {
                if (customer.Account.Equals(account) && customer.Pin.Equals(pin))
                {
                    return customer;
                }
            }
            throw new Exception("Log in error !");
        }
        public async Task Register(HttpClient client, Customer entity)
        {
            string urlPath = string.Concat(UrlRepository.url, UrlRepository.endPoint);
            await client.PostAsJsonAsync(urlPath, entity);
        }
    }
}
