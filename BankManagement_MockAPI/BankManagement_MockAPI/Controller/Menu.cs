﻿namespace BankManagement_MockAPI.Controller
{
    public class Menu
    {
        public void LogInMenu()
        {
            Console.WriteLine("==========Sign In==========");
            Console.WriteLine("| 1. Sign in              |");
            Console.WriteLine("| 2. Sign up              |");
            Console.WriteLine("| 3. Exit                 |");
            Console.WriteLine("===========================");
        }
        public void TransactionMenu()
        {
            Console.WriteLine("=========Transaction=========");
            Console.WriteLine("| 1. Recharge               |");
            Console.WriteLine("| 2. Withdraw               |");
            Console.WriteLine("| 3. Account balance        |");
            Console.WriteLine("| 4. Exit                   |");
            Console.WriteLine("=============================");
        }
        public int Input()
        {
            Console.WriteLine("Enter your choice: ");
            int choice = int.Parse(Console.ReadLine());
            return choice;
        }
    }
}
