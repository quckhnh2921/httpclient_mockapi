﻿using BankManagement_MockAPI.Interface;
using BankManagement_MockAPI.Model;
using BankManagement_MockAPI.Repository;
using System.Net.Http.Json;

namespace BankManagement_MockAPI.Controller
{
    public class Transaction : ITransaction<Customer>, UrlRepository
    {
        public void LogOut(HttpClient client)
        {
            client.Dispose();
        }
        public async Task Withdraw(HttpClient client, Customer entity)
        {
            string urlPath = string.Concat(UrlRepository.url, UrlRepository.endPoint + $"/{entity.Id}");
            entity.MakeTransaction();
            await client.PutAsJsonAsync(urlPath, entity);
        }
        public async Task Recharge(HttpClient client, Customer entity, decimal moneyRecharge)
        {
            string urlPath = string.Concat(UrlRepository.url, UrlRepository.endPoint + $"/{entity.Id}");
            entity.AccountBalance = entity.AccountBalance + moneyRecharge;
            await client.PutAsJsonAsync(urlPath, entity);
        }
        public decimal AccountBalance(Customer entity)
        {
            return entity.AccountBalance;
        }
    }
}
