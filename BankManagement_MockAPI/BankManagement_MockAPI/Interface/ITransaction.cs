﻿using BankManagement_MockAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankManagement_MockAPI.Interface
{
    public interface ITransaction<T> where T : class
    {
        void LogOut(HttpClient client);
        decimal AccountBalance(T entity);
        Task Recharge(HttpClient client, T entity, decimal moneyRecharge);
        Task Withdraw(HttpClient client, T entity);
    }
}
