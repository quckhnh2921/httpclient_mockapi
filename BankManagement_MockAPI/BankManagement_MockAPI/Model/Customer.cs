﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankManagement_MockAPI.Model
{
    public class Customer
    {
        private int id;
        private string name;
        private int age;
        private decimal accountBalance = 0;
        private decimal moneyWithdraw = 0;
        private string account;
        private string pin;

        public int Id
        {
            get => id;
            set => id = value;
        }
        public string Name
        {
            get => name;
            set => name = value;
        }
        public int Age
        {
            get => age;
            set
            {
                age = value;
                if (value < 18 || value > 100)
                {
                    throw new Exception("Age is invalid");
                }
            }
        }
        public decimal AccountBalance
        {
            get => accountBalance;
            set
            {
                accountBalance = value;
                if (value < 0)
                {
                    throw new Exception("Money is invalid");
                }
            }
        }
        public decimal MoneyWithdraw
        {
            get => moneyWithdraw;
            set
            {
                if (value < 0)
                {
                    throw new Exception("Withdraw must higher than 0");
                }
                if (value > accountBalance)
                {
                    throw new Exception("The account balance is not enough to make the transaction");
                }
                moneyWithdraw = value;
            }
        }
        public string Account
        {
            get => account;
            set
            {
                account = value;
                if (value.Length != 6)
                {
                    throw new Exception("Account is invalid");
                }
            }
        }
        public string Pin
        {
            get => pin;
            set
            {
                pin = value;
                if (value.Length != 6)
                {
                    throw new Exception("Pin is invalid");
                }
            }
        }
        public void MakeTransaction() => AccountBalance -= MoneyWithdraw;
    }
}
