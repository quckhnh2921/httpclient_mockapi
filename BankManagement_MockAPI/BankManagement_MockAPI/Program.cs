﻿using BankManagement_MockAPI.Controller;
using BankManagement_MockAPI.Interface;
using BankManagement_MockAPI.Model;

Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        HttpClient client = new HttpClient();
        Customer customer = new Customer();
        ITransaction<Customer> transaction = new Transaction();
        Login login = new Login();
        await login.GetData(client);
        menu.LogInMenu();
        choice = menu.Input();
        switch (choice)
        {
            case 1:
                Console.WriteLine("Enter user name: ");
                string account = Console.ReadLine();
                Console.WriteLine("Enter pin: ");
                string pin = Console.ReadLine();
                bool isLogin = login.LogIn(account, pin);
                if (isLogin)
                {
                    var customerLogin = login.GetCustomer(account, pin);
                    Console.WriteLine("Log in successful !");
                    do
                    {
                        try
                        {
                            menu.TransactionMenu();
                            choice = menu.Input();
                            switch (choice)
                            {
                                case 1:
                                    Console.WriteLine("Enter money recharge: ");
                                    decimal moneyRecharge = decimal.Parse(Console.ReadLine());
                                    await transaction.Recharge(client, customerLogin, moneyRecharge);
                                    break;
                                case 2:
                                    Console.WriteLine("Enter money withdraw: ");
                                    customerLogin.MoneyWithdraw = decimal.Parse(Console.ReadLine());
                                    await transaction.Withdraw(client, customerLogin);
                                    break;
                                case 3:
                                    var accountBalance = transaction.AccountBalance(customerLogin);
                                    Console.WriteLine(accountBalance);
                                    break;
                                case 4:
                                    transaction.LogOut(client);
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    } while (choice < 4);
                }
                break;
            case 2:
                Customer register = new Customer();
                Console.WriteLine("Enter user name: ");
                register.Account = Console.ReadLine();
                Console.WriteLine("Enter your pin: ");
                register.Pin = Console.ReadLine();
                Console.WriteLine("Enter your name: ");
                register.Name = Console.ReadLine();
                Console.WriteLine("Enter your age: ");
                register.Age = int.Parse(Console.ReadLine());
                await login.Register(client, register);
                break;
            case 3:
                Environment.Exit(0);
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 3);

