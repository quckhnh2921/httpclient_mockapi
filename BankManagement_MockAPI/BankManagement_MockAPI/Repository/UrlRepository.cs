﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankManagement_MockAPI.Repository
{
    public interface UrlRepository
    {
        protected const string url = "https://64c0b9550d8e251fd11271fe.mockapi.io/api/todoApp/";
        protected const string endPoint = "TransactionQueue";
    }
}
