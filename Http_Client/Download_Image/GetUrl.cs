﻿namespace Download_Image
{
    public class GetUrl
    {
        string directoryPath = @"Directory";
        List<string> urls;
        HttpClient client;
        public GetUrl()
        {
            client = new HttpClient();
            urls = new List<string>();
        }

        public void AddUrl()
        {
            Console.WriteLine("Enter url: ");
            string url = Console.ReadLine();
            if (url.Length is 0)
            {
                throw new Exception("Url is invalid");
            }
            urls.Add(url);
        }

        private async Task GetImage(string directoryPath, string url)
        {
            var data = await client.GetByteArrayAsync(url);
            string fileName = Path.GetFileName(url);
            string filePath = string.Concat(directoryPath, $"/{fileName}");
            await File.WriteAllBytesAsync(filePath, data);
        }

        public async Task DownloadImage()
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            foreach (string url in urls)
            {
                await GetImage(directoryPath, url);
            }
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}
