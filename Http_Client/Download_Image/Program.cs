﻿using Download_Image;
Menu menu = new Menu();
GetUrl url = new GetUrl();
int choice = 0;
do
{
    try
    {
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                url.AddUrl();
                break;
            case 2:
                Console.WriteLine("Waiting for download");
                await url.DownloadImage();
                Console.WriteLine("Download successful");
                break;
            case 3:
                Console.WriteLine("Bye bye");
                url.Dispose();
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 3");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 3);