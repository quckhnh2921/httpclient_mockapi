﻿using System.Net.Http.Json;
using System.Text;
using Newtonsoft.Json;
namespace Todo_MockApi
{
    public class Access
    {
        List<Todo> todoList;
        HttpClient client;
        const string url = @"https://64c0b9550d8e251fd11271fe.mockapi.io/api/todoApp/";
        const string endPoint = "Todo";
        public Access()
        {
            todoList = new List<Todo>();
        }
        public async Task ClientConnect()
        {
            client = new HttpClient();
            await GetData();
        }
        public async Task GetData()
        {
            string filePath = string.Concat(url, endPoint);
            try
            {
                string data = await client.GetStringAsync(filePath);
                todoList = JsonConvert.DeserializeObject<List<Todo>>(data);
            }
            catch (TaskCanceledException ex)
            {
                Console.WriteLine(ex);
            }
        }
        public async Task CreateTask(Todo todo)
        {
            string filePath = string.Concat(url, endPoint);
            await client.PostAsJsonAsync(filePath, todo);
        }
        public async Task UpdateTask(int id)
        {
            string filePath = string.Concat(url, endPoint + $"/{id}");
            var todoUpdate = todoList.Find(todo => todo.Id == id);
            if (todoUpdate is null)
            {
                throw new Exception("Can not found task by id " + id);
            }
            todoUpdate.IsComplete = true;
            todoUpdate.FinalAt = DateTime.Now;
            await client.PutAsJsonAsync(filePath, todoUpdate);
        }
        public async Task DeleteTask(int id)
        {
            var todoRemove = todoList.Find(todo => todo.Id == id);
            if (todoRemove is null)
            {
                throw new Exception("Can not found task by id "+id);
            }
            string filePath = string.Concat(url, endPoint + $"/{id}");
            await client.DeleteAsync(filePath);
        }
        public string PrintCompleteTask()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in todoList)
            {
                if (item.IsComplete is true)
                {
                    sb.Append($"\n{item.Id} | {item.Title} | Create at: {item.CreateAt.ToString("hh-mm dd/MM/yyyy")} | Completed at:  {item.FinalAt?.ToString("hh-mm dd/MM/yyyy ")}");
                }
            }
            if(sb.Length == 0)
            {
                throw new Exception("List is empty");
            }
            return sb.ToString();
        }

        public string PrintUnCompletedTask()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in todoList)
            {
                if (item.IsComplete is false)
                {
                    sb.Append($"\n{item.Id} | {item.Title} | Create at: {item.CreateAt.ToString("hh-mm dd/MM/yyyy")}");
                }
            }
            if (sb.Length == 0)
            {
                throw new Exception("List is empty");
            }
            return sb.ToString();
        }
        public void Dispose()
        {
            client.Dispose();
        }
    }
}
