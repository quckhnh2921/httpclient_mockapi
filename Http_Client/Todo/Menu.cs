﻿namespace Todo_MockApi
{
    public class Menu
    {
        public void MainMenu()
        {
            Console.WriteLine("===========MENU=============");
            Console.WriteLine("| 1. Create task          |");
            Console.WriteLine("| 2. Complete task        |");
            Console.WriteLine("| 3. Delete task          |");
            Console.WriteLine("| 4. Print complete task  |");
            Console.WriteLine("| 5. Print uncomplete task|");
            Console.WriteLine("| 6. Exit                 |");
            Console.WriteLine("===========================");
            Console.Write("Choice ");
        }
    }
}
