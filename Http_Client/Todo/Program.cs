﻿using Todo_MockApi;
Menu menu = new Menu();
Access access = new Access();
int choice = 0;
do
{
    await access.ClientConnect();
    try
    {
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Todo todo = new Todo();
                Console.WriteLine("Enter task: ");
                todo.Title = Console.ReadLine();
                await access.CreateTask(todo);
                access.Dispose();
                break;
            case 2:
                Console.WriteLine("Enter id to complete: ");
                int idUpdate = int.Parse(Console.ReadLine());
                await access.UpdateTask(idUpdate);
                Console.WriteLine("Update successful");
                access.Dispose();
                break;
            case 3:
                Console.WriteLine("Enter id to remove: ");
                int idRemove = int.Parse(Console.ReadLine());
                await access.DeleteTask(idRemove);
                Console.WriteLine("Remove successful");
                access.Dispose();
                break;
            case 4:
                var completeList = access.PrintCompleteTask();
                Console.WriteLine(completeList);
                access.Dispose();
                break;
            case 5:
                var uncompleteList = access.PrintUnCompletedTask();
                Console.WriteLine(uncompleteList);
                access.Dispose();
                break;
            case 6:
                Console.WriteLine("Bye bye");
                access.Dispose();
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 6");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);