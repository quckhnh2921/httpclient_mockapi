﻿namespace Todo_MockApi
{
    public class Todo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public DateTime? FinalAt { get; set; } = null;
        public bool IsComplete { get; set; } = false;
    }
}
