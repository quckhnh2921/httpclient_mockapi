﻿namespace Image_Collection
{
    public interface IRepository
    {
        Task Add (Image image);
        Task Get();
        Task DownLoad(int id);
        string ShowListImage();
        void Dispose();
        Task IsDownload(int id);
    }
}
