﻿using Image_Collection;
Menu menu = new Menu();
IRepository repo = new DownloadImage();
int choice = 0;
do
{
    try
    {
        await repo.Get();
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine("Enter image's link: ");
                Image image = new Image();
                Console.WriteLine("Enter url: ");
                image.Url = Console.ReadLine();
                Console.WriteLine("Enter Description: ");
                image.Description = Console.ReadLine();
                await repo.Add(image);
                break;
            case 2:
                Console.WriteLine(repo.ShowListImage());
                Console.WriteLine("Enter id to download");
                int id = int.Parse(Console.ReadLine());
                await repo.DownLoad(id);
                await repo.IsDownload(id);
                Console.WriteLine("Download successful");
                break;
            case 3:
                repo.ShowListImage();
                break;
            case 4:
                repo.Dispose();
                Environment.Exit(0);
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);